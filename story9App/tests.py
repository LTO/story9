from django.test import TestCase
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import loginpage, halopage

# Create your tests here.

class story9UnitTest(TestCase):

    def test_ada_loginpage(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_apakah_menggunakan_fungsi_loginpage(self):
        found = resolve('/')
        self.assertEqual(found.func, loginpage)

    def test_url_halopage(self):
        response = Client().get('/halopage/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'halopage.html')

    def test_button_logout(self):
        c = Client()
        response = c.get('/halopage/')
        content = response.content.decode('utf8') 
        self.assertIn ("<button", content) 
        self.assertIn("Logout", content)

