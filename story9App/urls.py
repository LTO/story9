from django.contrib import admin
from django.urls import path, include
from . import views

app_name = 'story9App'

urlpatterns = [
    path('', views.loginpage, name='loginpage'),
    path('halopage/', views.halopage, name='halopage'),
    path('logoutUser/', views.logoutUser, name='logoutUser'),

]
