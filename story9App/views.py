from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse


# Create your views here.

def halopage(request):
    return render(request, 'halopage.html')


def loginpage(request):
    context ={}
    if request.method == "POST" :
        name = request.POST['username']
        passw = request.POST['password']
        user = authenticate(request, username=name, password=passw)
        if user:
            login(request, user)
            return HttpResponseRedirect(reverse('story9App:halopage'))

        else:
            context["user_not_found"]= "User not found."
            return render(request, 'loginpage.html', context)
    else:
        return render(request, 'loginpage.html', context)

def halopage(request): 
    context = {}
    context['user'] = request.user
    return render(request, 'halopage.html', context)

def logoutUser(request):
    if request.method =="POST":
        logout(request)
        return HttpResponseRedirect(reverse('story9App:loginpage'))
